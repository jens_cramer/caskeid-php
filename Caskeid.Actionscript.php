<?
// Aktion-Skript f�r �nderungen an Caskeid-Devices �ber das Webfront

include("caskeid.class.php");

if ($_IPS['SENDER'] == "WebFront") {

   $value = $_IPS['VALUE'];

	$ident  = IPS_GetObject($_IPS['VARIABLE']);
	$ident  = $ident['ObjectIdent'];
	
	$parent = IPS_GetParent($_IPS['VARIABLE']);
	$ip     = GetValue(IPS_GetObjectIDByIdent("IP", $parent));
	$port   = GetValue(IPS_GetObjectIDByIdent("PORT", $parent));
	$box    = new CaskeidUpnpDevice("http://".$ip.":".$port);

	// BTCaskeid
	if ($ident == "BTCASKEID") {
		IPS_LogMessage("BTcaske",$box->CallService('SpeakerManagement','EnableBTCaskeid',array($value)));
	}
	
	// Volume
   if ($ident == "VOLUME") {
		$resp = $box->CallService('RenderingControl','SetVolume',array($value));
		IPS_LogMessage("Volume", $resp);
	}

	SetValue($_IPS['VARIABLE'],$_IPS['VALUE']);
}
?>
