<?
/*****************************
*
* Caskeid.Subscribe.SpeakerManagement.php
*
* Baut Subscribtionen auf zu den SessionManagement Services.
* Muss mind. alle 5 Minuten wiederholt werden.
*
* (c) 2015 Jens-Michael Cramer
*
* Version: 1.0
*
******************************/
set_time_limit(120);
require_once("caskeid.class.php");


$callback_url = "http://192.168.11.4/speakermanagement.php";
$callback_url = "http://192.168.11.23:82/hook/caskeid_speaker";

$speaker = IPS_GetChildrenIDs(CaskeidUpnpDevice::getDeviceFolderID());

foreach($speaker as $s) {
	$ip   = GetValueString(IPS_GetObjectIDByIdent("IP",$s));
	$port = GetValueString(IPS_GetObjectIDByIdent("PORT",$s));

	$box = new CaskeidUpnpDevice("http://".$ip.":".$port);
	try {
		$box->CallService('SpeakerManagement','UnRegisterEventCallback',"");
	} catch (Exception $e) {}
	try {
	   $cb_url = $callback_url . $ip;
		#$box->CallService('SpeakerManagement','RegisterEventCallback',array($cb_url,300));
		$box->CallService('SpeakerManagement','RegisterEventCallback',array($callback_url,300));
	} catch (Exception $e) {}
}
?>
