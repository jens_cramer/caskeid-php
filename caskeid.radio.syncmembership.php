<?
// Gleicht den Mitgliedsstatus in einer Session mit den tatsächlichen Gegebenheiten ab.

$var = $_IPS['VARIABLE'];
$parent = IPS_GetParent($var);
$parentname = IPS_GetObject($parent)['ObjectName'];
$memberfolder = IPS_GetChildrenIDs(IPS_GetObjectIDByName("Member", IPS_GetParent($_IPS['SELF'])))[0];
$status = IPS_GetObjectIDByName("Status",IPS_GetChildrenIDs(IPS_GetObjectIDByName("Controls", IPS_GetParent($_IPS['SELF'])))[0]);

if (IPS_GetObject($var)['ObjectIdent'] == "SESSIONID") {
	// Session-ID hat sich verändert
	if (!$_IPS['VALUE']) {
		// Speaker ist nicht mehr in Session, deaktivieren
		$speaker = IPS_GetObjectIDByName($parentname, $memberfolder);
		SetValueBoolean($speaker, false);
		// Events löschen
		IPS_DeleteEvent(IPS_GetObjectIDByName("SESS_".$parentname, IPS_GetParent($_IPS['SELF'])));
		// Noch ein Lautsprecher aktiv?
		$playing = 0;
		foreach (IPS_GetChildrenIDs($memberfolder) as $member) {
			if (GetValue($member)) {
				$playing++;
			}
		}
		if (!$playing) {
			SetValueInteger($status, 2);
		}
	}
}
IPS_LogMessage("RADIO MEMBER", print_r($_IPS,true));
?>
