<?
/**
 * Dieses Skript installiert die Caskeid-Funktionalität in IP-Symcon
 *
 * (c) 2015, Jens Cramer
 *
 * Die Skript-Dateien müssen in $IPS$\scripts\caskeid liegen!
 *
 * Vor der Installation muss in IPS eine Kategorie angelegt werden,
 * deren ID in die unten stehende Variable $base einzutragen ist.
 */

$base = 12345; // Bitte ändern auf die Kategorie, in die die Skripte installiert werden sollen
$webfront = 98765; // ID des Webfront-Konfigurators in dem Caskeid-Radio installiert werden soll.

/*
 * Ab hier nichts mehr verändern!
 */
$path = 'caskeid\\';

// Haupt-Klasse in IPS integrieren
$name = "caskeid.class.php";
$main = IPS_CreateScript(0);
IPS_SetParent($main, $base);
IPS_SetName($main, $name);
IPS_SetScriptFile($main,  $path . $name);

// Session-Klasse in IPS integrieren
$name = "caskeid.session.class.php";
$sess = IPS_CreateScript(0);
IPS_SetParent($sess, $base);
IPS_SetName($sess, $name);
IPS_SetScriptFile($sess,  $path . $name);

// Management-Ordner anlegen
$manage = IPS_CreateCategory();
IPS_SetParent($manage, $base);
IPS_SetName($manage, "Management");

// AVTransport-Subscribe in IPS integrieren
$name = "Caskeid.Subscribe.AVTransport.php";
$avtrans = IPS_CreateScript(0);
IPS_SetParent($avtrans, $manage);
IPS_SetName($avtrans, $name);
IPS_SetScriptFile($avtrans,  $path . $name);

// RenderingControl-Subscribe in IPS integrieren
$name = "Caskeid.Subscribe.RenderingControl.php";
$render = IPS_CreateScript(0);
IPS_SetParent($render, $manage);
IPS_SetName($render, $name);
IPS_SetScriptFile($render,  $path . $name);

// SessionManagement-Subscribe in IPS integrieren
$name = "Caskeid.Subscribe.SessionManagement.php";
$sesssub = IPS_CreateScript(0);
IPS_SetParent($sesssub, $manage);
IPS_SetName($sesssub, $name);
IPS_SetScriptFile($sesssub,  $path . $name);

// SpeakerManagement-Subscribe in IPS integrieren
$name = "Caskeid.Subscribe.SpeakerManagement.php";
$speakermgm = IPS_CreateScript(0);
IPS_SetParent($speakermgm, $manage);
IPS_SetName($speakermgm, $name);
IPS_SetScriptFile($speakermgm,  $path . $name);

// Actionskript in IPS integrieren
$name = "Caskeid.Actionscript.php";
$action = IPS_CreateScript(0);
IPS_SetParent($action, $manage);
IPS_SetName($action, $name);
IPS_SetScriptFile($action,  $path . $name);

// Port-Updater in IPS integrieren
$name = "Caskeid.UpdatePort.Management.ips.php";
$port = IPS_CreateScript(0);
IPS_SetParent($port, $manage);
IPS_SetName($port, $name);
IPS_SetScriptFile($port,  $path . $name);

// Webhook-Ordner anlegen
$webhooks = IPS_CreateCategory();
IPS_SetParent($manage, $base);
IPS_SetName($manage, "Webhooks");

// AVTransport-Webhook in IPS integrieren
$name = "Caskeid.Webhook.AVTrans.ips.php";
$avtrans_WH = IPS_CreateScript(0);
IPS_SetParent($avtrans_WH, $webhooks);
IPS_SetName($avtrans_WH, $name);
IPS_SetScriptFile($avtrans_WH,  $path . $name);

// Rendering-Webhook in IPS integrieren
$name = "Caskeid.Webhook.Rendering.ips.php";
$render_WH = IPS_CreateScript(0);
IPS_SetParent($render_WH, $webhooks);
IPS_SetName($render_WH, $name);
IPS_SetScriptFile($render_WH,  $path . $name);

// Session-Webhook in IPS integrieren
$name = "Caskeid.Webhook.Sessionmgm.ips.php";
$session_WH = IPS_CreateScript(0);
IPS_SetParent($session_WH, $webhooks);
IPS_SetName($session_WH, $name);
IPS_SetScriptFile($session_WH,  $path . $name);

// Speaker-Webhook in IPS integrieren
$name = "Caskeid.Webhook.Speakermgm.ips.php";
$speaker_WH = IPS_CreateScript(0);
IPS_SetParent($speaker_WH, $webhooks);
IPS_SetName($speaker_WH, $name);
IPS_SetScriptFile($speaker_WH,  $path . $name);

// Events anlegen
$avevent = IPS_CreateEvent(1);
IPS_SetParent($avevent, $avtrans);
IPS_SetEventCyclic($avevent, 0, 0, 0, 0, 1, 240);
IPS_SetEventActive($avevent, true);

$renderevent = IPS_CreateEvent(1);
IPS_SetParent($renderevent, $render);
IPS_SetEventCyclic($renderevent, 0, 0, 0, 0, 1, 240);
IPS_SetEventActive($renderevent, true);

$sesssubevent = IPS_CreateEvent(1);
IPS_SetParent($sesssubevent, $sesssub);
IPS_SetEventCyclic($sesssubevent, 0, 0, 0, 0, 1, 240);
IPS_SetEventActive($sesssubevent, true);

$speakermgmevent = IPS_CreateEvent(1);
IPS_SetParent($speakermgmevent, $speakermgm);
IPS_SetEventCyclic($speakermgmevent, 0, 0, 0, 0, 1, 240);
IPS_SetEventActive($speakermgmevent, true);

$portevent = IPS_CreateEvent(1);
IPS_SetParent($portevent, $port);
IPS_SetEventCyclic($portevent, 0, 0, 0, 0, 2, 15);
IPS_SetEventActive($portevent, true);

// Kategorie für die Speaker-Objekte anlegen
$speakercat = IPS_CreateCategory();
IPS_SetParent($speakercat, $base);
IPS_SetName($speakercat, "Speaker");

// WebFront-Radio
$radiocat = IPS_CreateCategory();
IPS_SetParent($radiocat, $base);
IPS_SetName($radiocat, "Webfront-Radio");

$name = "caskeid.radio.memberupdate.php";
$radiomemberskript = IPS_CreateScript(0);
IPS_SetParent($radiomemberskript, $radiocat);
IPS_SetName($radiomemberskript, $name);
IPS_SetScriptFile($radiomemberskript,  $path . $name);

$name = "caskeid.radiocontroller.php";
$radionconrollerskript = IPS_CreateScript(0);
IPS_SetParent($radionconrollerskript, $radiocat);
IPS_SetName($radionconrollerskript, $name);
IPS_SetScriptFile($radionconrollerskript,  $path . $name);

$radiovolumecat = IPS_CreateCategory();
IPS_SetParent($radiovolumecat, $radiocat);
IPS_SetName($radiovolumecat, "Member Volume");

$radiovolumeinstance = IPS_CreateInstance("{485D0419-BE97-4548-AA9C-C083EB82E61E}");
IPS_SetParent($radiovolumeinstance, $radiovolumecat);
IPS_SetName($radiovolumeinstance, "Lautstärke");

$radiomembercat = IPS_CreateCategory();
IPS_SetParent($radiomembercat, $radiocat);
IPS_SetName($radiomembercat, "Members");

$radiocontrolscat = IPS_CreateCategory();
IPS_SetParent($radiocontrolscat, $radiocat);
IPS_SetName($radiocontrolscat, "Controls");

$radiocontrolinstance = IPS_CreateInstance("{485D0419-BE97-4548-AA9C-C083EB82E61E}");
IPS_SetParent($radiocontrolinstance, $radiocontrolscat);
IPS_SetName($radiocontrolinstance, "Steuerung");

$sendervar = IPS_CreateVariable(3);
IPS_SetParent($sendervar, $radiocontrolinstance);
IPS_SetName($sendervar, "Sender");
IPS_SetIdent($sendervar, "SENDER");
IPS_SetPosition($sendervar, 100);
SetValue($sendervar, "");

$sendervarevent = IPS_CreateEvent(0);
IPS_SetEventActive($sendervarevent, true);
IPS_SetParent($sendervarevent, $radionconrollerskript);
IPS_SetEventTrigger($sendervarevent, 1, $sendervar);

if (!IPS_VariableProfileExists("_CaskeidPlayStop") ) {
    IPS_CreateVariableProfile("_CaskeidPlayStop", 1);
    IPS_SetVariableProfileValues("_CaskeidPlayStop",0,0,0);
    IPS_SetVariableProfileAssociation("_CaskeidPlayStop", 0, "", "Power", -1);
    IPS_SetVariableProfileAssociation("_CaskeidPlayStop", 1, "Play", "ArrowRight", 0x00FF00);
    IPS_SetVariableProfileAssociation("_CaskeidPlayStop", 2, "Stop", "", 0xFF0000);
}

$statusvar = IPS_CreateVariable(1);
IPS_SetParent($statusvar, $radiocontrolinstance);
IPS_SetName($statusvar, "Status");
IPS_SetIdent($statusvar, "STATUS");
IPS_SetPosition($statusvar, 200);
SetValue($statusvar, 2);
IPS_SetVariableCustomAction($volumevar, $radionconrollerskript);
IPS_SetVariableCustomProfile($volumevar, "_CaskeidPlayStop");

$volumevar = IPS_CreateVariable(1);
IPS_SetParent($volumevar, $radiocontrolinstance);
IPS_SetName($volumevar, "Lautstärke");
IPS_SetIdent($volumevar, "VOLUME");
IPS_SetPosition($volumevar, 300);
SetValue($volumevar, 11);
IPS_SetVariableCustomAction($volumevar, $radionconrollerskript);
IPS_SetVariableCustomProfile($volumevar, "~Intensity.100");


// Webfront bauen
CreateWFCItemTabPane($webfront, "caskeid", "roottp", 999, "Caskeid", "Speaker");
CreateWFCItemSplitPane($webfront, "caskeidradio","caskeid",0,"Radio","",0,365,0,1);
CreateWFCItemCategory($webfront, "caskeidspeaker","caskeid",0,"Speaker","",$speakercat);
CreateWFCItemSplitPane($webfront, "caskeidradio2","caskeidradio",0,"","",0,145,0,1,"false");
CreateWFCItemSplitPane($webfront, "caskeidradio3","caskeidradio2",1,"","",1,50,0,0,"false");
CreateWFCItemCategory($webfront, "caskeidradiocontrols","caskeidradio2",0,"","",$radiocontrolscat);
CreateWFCItemCategory($webfront, "caskeidradiomember","caskeidradio3",0,"","",$radiomembercat);
CreateWFCItemCategory($webfront, "caskeidradiomembervol","caskeidradio3",1,"","",$radiovolumecat);
CreateWFCItemExternalPage($webfront,"caskeidradiochooser","caskeidradio",1,"","","user/caskeidradio/radio.php", 'false');

// TODO Radio-Skripte nach /webfront/user kopieren

//
// Config-Datei schreiben (und ggf. vorher eine vorhandene löschen)
//
$config = IPS_GetKernelDir()."/scripts/caskeid/caskeid.conf.php";
$nl = chr(13).chr(10);
if (file_exists($config)) {
   unlink($config);
}
$conf = "<?$nl"."define('CASKEID_SPEAKER_PATH',$speakercat);$nl"."define('CASKEID_ACTIONSCRIPT',$action);$nl"."define('CASKEID_RADIO_VOLUME_ID',$volumevar);$nl"."?>";
file_put_contents($config, $conf);

//
// Alle Komponenten sind jetzt soweit angelegt.
// Nun erstellen wir noch die Lautsprecher-Objekte
//
include($path."caskeid.class.php");
CaskeidUpnpDevice::updatePorts(); // Einmal Update der Ports (obwohl ja noch keine Objekte da sind), damit die Speaker alle aufwachen
CaskeidUpnpDevice::findAndCreateDevices();

// Anlegen der Links in den Radio-Kategorien
$speakers = IPS_GetChildrenIDs($speakercat);
foreach($speakers as $s) {
    // Lautstärke / Volume
    $vol = IPS_GetObjectIDByIdent("VOLUME", $s);
    $name = GetValue(IPS_GetObjectIDByIdent("NAME", $s));
    $link = IPS_CreateLink();
    IPS_SetLinkTargetID($link, $vol);
    IPS_SetName ($link, $name);
    
    #$event = IPS_CreateEvent(0);
    #IPS_SetEventActive($event, true);
    #IPS_SetParent($event, $radionconrollerskript);
    #IPS_SetEventTrigger($event, 1, $sendervar);
}

/*
 * Die nachfolgenden Funktionen habe ich dem Install-Skript von der IPSLibrary
 * von Andreas Brauneis entnommen. Ich hoffe das geht in Ordnung Andreas?
 */
    function PrepareWFCItemData (&$ItemId, &$ParentId, &$Title) {
		$ItemId   = str_replace(' ','_',$ItemId);
		$ParentId = str_replace(' ','_',$ParentId);
		//$ItemId   = str_replace('_','',$ItemId);
		//$ParentId = str_replace('_','',$ParentId);
		$version = IPS_GetKernelVersion();
		$versionArray = explode('.', $version);
		if ($versionArray[0] < 3) {
			$Title    = utf8_encode($Title);
			$ItemId   = utf8_encode($ItemId);
			$ParentId = utf8_encode($ParentId);
		}
	}

	function CreateWFCItem ($WFCId, $ItemId, $ParentId, $Position, $Title, $Icon, $ClassName, $Configuration) {
	   if (!exists_WFCItem($WFCId, $ItemId)) {
		   Debug ("Add WFCItem='$ItemId', Class=$ClassName, Config=$Configuration");
			WFC_AddItem($WFCId, $ItemId, $ClassName, $Configuration, $ParentId);
		}
		WFC_UpdateConfiguration($WFCId, $ItemId, $Configuration);
		WFC_UpdateParentID($WFCId, $ItemId, $ParentId);
		WFC_UpdatePosition($WFCId,$ItemId, $Position);
		IPS_ApplyChanges($WFCId);
	}

	/** Anlegen eines TabPanes im WebFront Konfigurator
	 *
	 * Der Befehl legt im WebFront Konfigurator ein TabPane mit dem Element Namen $ItemId an
	 *
	 * @param integer $WFCId ID des WebFront Konfigurators
	 * @param string $ItemId Element Name im Konfigurator Objekt Baum
	 * @param string $ParentId Übergeordneter Element Name im Konfigurator Objekt Baum
	 * @param integer $Position Positionswert im Objekt Baum
	 * @param string $Title Title
	 * @param string $Icon Dateiname des Icons ohne Pfad/Erweiterung
	 *
	 */
	function CreateWFCItemTabPane ($WFCId, $ItemId, $ParentId, $Position, $Title, $Icon) {
		PrepareWFCItemData ($ItemId, $ParentId, $Title);
		$Configuration = "{\"title\":\"$Title\",\"name\":\"$ItemId\",\"icon\":\"$Icon\"}";
		CreateWFCItem ($WFCId, $ItemId, $ParentId, $Position, $Title, $Icon, 'TabPane', $Configuration);
	}

	/** Anlegen eines SplitPanes im WebFront Konfigurator
	 *
	 * Der Befehl legt im WebFront Konfigurator ein SplitPane mit dem Element Namen $ItemId an
	 *
	 * @param integer $WFCId ID des WebFront Konfigurators
	 * @param string $ItemId Element Name im Konfigurator Objekt Baum
	 * @param string $ParentId Übergeordneter Element Name im Konfigurator Objekt Baum
	 * @param integer $Position Positionswert im Objekt Baum
	 * @param string $Title Title
	 * @param string $Icon Dateiname des Icons ohne Pfad/Erweiterung
	 * @param integer $Alignment Aufteilung der Container (0=horizontal, 1=vertical)
	 * @param integer $Ratio Größe der Container
	 * @param integer $RatioTarget Zuordnung der Größenangabe (0=erster Container, 1=zweiter Container)
	 * @param integer $RatioType Einheit der Größenangabe (0=Percentage, 1=Pixel)
	 * @param string $ShowBorder Zeige Begrenzungs Linie
	 *
	 */
	function CreateWFCItemSplitPane ($WFCId, $ItemId, $ParentId, $Position, $Title, $Icon="", $Alignment=0 /*0=horizontal, 1=vertical*/, $Ratio=50, $RatioTarget=0 /*0 or 1*/, $RatioType /*0=Percentage, 1=Pixel*/, $ShowBorder='true' /*'true' or 'false'*/) {
		PrepareWFCItemData ($ItemId, $ParentId, $Title);
		$Configuration = "{\"title\":\"$Title\",\"name\":\"$ItemId\",\"icon\":\"$Icon\",\"alignmentType\":$Alignment,\"ratio\":$Ratio,\"ratioTarget\":$RatioTarget,\"ratioType\":$RatioType,\"showBorder\":$ShowBorder}";
		CreateWFCItem ($WFCId, $ItemId, $ParentId, $Position, $Title, $Icon, 'SplitPane', $Configuration);
	}

	/** Anlegen einer Kategorie im WebFront Konfigurator
	 *
	 * Der Befehl legt im WebFront Konfigurator eine Kategorie mit dem Element Namen $ItemId an
	 *
	 * @param integer $WFCId ID des WebFront Konfigurators
	 * @param string $ItemId Element Name im Konfigurator Objekt Baum
	 * @param string $ParentId Übergeordneter Element Name im Konfigurator Objekt Baum
	 * @param integer $Position Positionswert im Objekt Baum
	 * @param string $Title Title
	 * @param string $Icon Dateiname des Icons ohne Pfad/Erweiterung
	 * @param integer $BaseId Kategorie ID im logischen Objektbaum
	 * @param string $BarBottomVisible Sichtbarkeit der Navigations Leiste
	 * @param integer $BarColums
	 * @param integer $BarSteps
	 * @param integer $PercentageSlider
	 *
	 */
	function CreateWFCItemCategory ($WFCId, $ItemId, $ParentId, $Position, $Title, $Icon="", $BaseId /*ID of Category*/, $BarBottomVisible='true' /*'true' or 'false'*/, $BarColums=9, $BarSteps=5, $PercentageSlider='true' /*'true' or 'false'*/ ) {
		PrepareWFCItemData ($ItemId, $ParentId, $Title);
		$Configuration = "{\"title\":\"$Title\",\"name\":\"$ItemId\",\"icon\":\"$Icon\",\"baseID\":$BaseId,\"enumBarColumns\":$BarColums,\"selectorBarSteps\":$BarSteps,\"isBarBottomVisible\":$BarBottomVisible,\"enablePercentageSlider\":$PercentageSlider}";
		CreateWFCItem ($WFCId, $ItemId, $ParentId, $Position, $Title, $Icon, 'Category', $Configuration);
	}

	/** Anlegen einer ExternalPage im WebFront Konfigurator
	 *
	 * Der Befehl legt im WebFront Konfigurator eine ExternalPage mit dem Element Namen $ItemId an
	 *
	 * @param integer $WFCId ID des WebFront Konfigurators
	 * @param string $ItemId Element Name im Konfigurator Objekt Baum
	 * @param string $ParentId Übergeordneter Element Name im Konfigurator Objekt Baum
	 * @param integer $Position Positionswert im Objekt Baum
	 * @param string $Title Title
	 * @param string $Icon Dateiname des Icons ohne Pfad/Erweiterung
	 * @param string $PageUri URL der externen Seite
	 * @param string $BarBottomVisible Sichtbarkeit der Navigations Leiste
	 *
	 */
	function CreateWFCItemExternalPage ($WFCId, $ItemId, $ParentId, $Position, $Title, $Icon="", $PageUri, $BarBottomVisible='true' /*'true' or 'false'*/) {
		PrepareWFCItemData ($ItemId, $ParentId, $Title);
		$Configuration = "{\"title\":\"$Title\",\"name\":\"$ItemId\",\"icon\":\"$Icon\",\"pageUri\":\"$PageUri\",\"isBarBottomVisible\":$BarBottomVisible}";
		CreateWFCItem ($WFCId, $ItemId, $ParentId, $Position, $Title, $Icon, 'ExternalPage', $Configuration);
	}

    /** Existenz eines WebFront Konfigurator Items überprüfen
	 *
	 * Der Befehl überprüft ob ein bestimmtes Item im WebFront Konfigurator existiert
	 *
	 * @param integer $WFCId ID des WebFront Konfigurators
	 * @param string $ItemId Element Name im Konfigurator Objekt Baum
	 * @return boolean TRUE wenn das Item existiert anderenfalls FALSE
	 *
	 */
	function exists_WFCItem($WFCId, $ItemId) {
	   $ItemList = WFC_GetItems($WFCId);
	   foreach ($ItemList as $Item) {
	      if ($Item['ID']==$ItemId) {
				return true;
	      }
	   }
	   return false;
	}
?>
