<?
/*---------------------------------------------------------------------------/

File:
	Desc     : PHP Class to Control Caskeid Sessions
	Version  : 0.50
	Publisher: (c)2015 Jens-Michael Cramer
	Contact  : jmc@jmc.bz

/*--------------------------------------------------------------------------*/
set_time_limit(120);
require_once("caskeid.class.php");
/*##########################################################################/
/*  Class  : CaskeidSession
/*  Desc   : Master Class to Controll Sessions
/*##########################################################################*/
if (!class_exists("CaskeidSession") ) {
	class CaskeidSession {
	
        private $_MEMBERS   = array();
        private $_MASTER    = null;
        private $_SESSIONID = null;

        /**
         * Checks for existing session and configures itself acordingly.
         * If there is no session we'll create an empty one.
         */
        public function __construct($sessionid = ""){
            foreach(CaskeidUpnpDevice::getDeviceArray() as $name => $ip) {
                $speaker = CaskeidUpnpDevice::getInstanceByName($name);
                if ($speaker instanceof CaskeidUpnpDevice) {
            		$this->getSessionStatusForObject($speaker, $sessionid);
            	}
            }
            if (!$this->_MASTER && sizeof($this->_MEMBERS)) {
            	// There is no active master but some abandonded Session-Members.
            	// Kill them
            	foreach($this->_MEMBERS as $m) {
            	    $s = $m->getSessionID();
            		$m->CallService("SpeakerManagement","ClearSession", array( $s ));
            		IPS_LogMessage("Caskeid Session", "Session cleared");
            	}
            }
        }


        private function getSessionStatusForObject($speaker_object, $sessionid = "") {
        if ($speaker_object->getSessionID()) {
           if ($sessionid && $speaker_object->getSessionID() != $sessionid) {
        		return "";
        	}
        	$this->_MEMBERS[] = $speaker_object;
        	if ($speaker_object->GetIsSessionMaster()) {
        		$this->_MASTER = $speaker_object;
        		$this->_SESSIONID = $speaker_object->getSessionID();
        	}
        }
        }
        
        public function GetSessionID() {
				return $this->$_SESSIONID;
		  }

        public function GetMemberStatusByName($name) {
            foreach($this->_MEMBERS as $member) {
            	if ($member->GetName() == $name) {
            		return $member;
            	}
            }
            return false;
        }

        public function AddMemberByName($name) {
            if (!$this->GetMemberStatusByName($name)) {
                $speaker = CaskeidUpnpDevice::getInstanceByName($name);
                if (!($this->_MASTER instanceof CaskeidUpnpDevice)) {
            		$this->_SESSIONID = $speaker->CreateSession();
            		$this->_MASTER = $speaker;
            		$this->_MEMBERS[] = $speaker;
            	} else {
            	    $speaker_session = $speaker->getSessionID();
            	    if ($speaker_session && $speaker_session != $this->_SESSIONID) {
            			// Speaker is already part of a session, clear this
            			$speaker->CallService("SpeakerManagement","ClearSession", array( $speaker_session ));
            		}
            		$this->_MASTER->AddUnitToSession($speaker->GetUUID());
            		$this->_MEMBERS[] = $speaker;
            	}
            }
        }
        
        public function AddAllSpeakers() {
            foreach(CaskeidUpnpDevice::getDeviceArray() as $name => $ip) {
					$this->AddMemberByName($name);
				}
		  }

        public function RemoveMemberByName($name) {
            if ($this->GetMemberStatusByName($name)) {
               $speaker = CaskeidUpnpDevice::getInstanceByName($name);
               if ($this->_MASTER instanceof CaskeidUpnpDevice) {
               	$this->_MASTER->RemoveUnitFromSession( $speaker->GetUUID() );
            		foreach ($this->_MEMBERS as $key => $object) {
            			if ($object->GetUUID() == $speaker->GetUUID()) {
                  		unset($this->_MEMBERS[$key]);
                        break;
            			}
            		}
            	}
            }
        }

        public function DestroySession($destroy_all = false) {
            if (!($this->_MASTER instanceof CaskeidUpnpDevice)) {
            	$this->_MASTER = null;
            	$this->_MEMBERS = array();
            	$this->_SESSIONID = null;
            } else {
            	foreach($this->_MEMBERS as $member) {
            		if ($member->GetUUID() != $this->_MASTER->GetUUID()) {
            			$this->_MASTER->RemoveUnitFromSession($member->GetUUID());
            		}
            	}
            	$session = $this->_SESSIONID;
            	$this->_MASTER->CallService("SessionManagement","DestroySession", array( $session ));
            	$this->_MASTER = null;
            	$this->_MEMBERS = array();
            	$this->_SESSIONID = null;
            }
            if ($destroy_all) {
                foreach(CaskeidUpnpDevice::getDeviceArray() as $name => $ip) {
                    $speaker = CaskeidUpnpDevice::getInstanceByName($name);
                    if ($speaker instanceof CaskeidUpnpDevice) {
						$speaker_session = $speaker->getSessionID();
					    if ($speaker_session) {
							// Speaker is part of a session, clear this
                            $speaker->CallService("AVTransport","Stop", array());
							$speaker->CallService("SpeakerManagement","ClearSession", array( $speaker_session ));
					    }
                    }
                }
            }
        }

        public function SetAVTransportURI($url) {
            if ($this->_MASTER instanceof CaskeidUpnpDevice) {
            	$this->_MASTER->CallService("ConnectionManager","PrepareForConnection", array());
            	return $this->_MASTER->CallService("AVTransport","SetAVTransportURI", array($url));
            } else {
            	return false;
            }
        }

        public function Play() {
            if ($this->_MASTER instanceof CaskeidUpnpDevice) {
            	return $this->_MASTER->CallService("AVTransport","Play", array());
            } else {
            	return false;
            }
        }

        public function Stop() {
            if ($this->_MASTER instanceof CaskeidUpnpDevice) {
            	return $this->_MASTER->CallService("AVTransport","Stop", array());
            } else {
            	return false;
            }
        }

        public function StartPlaybackFromURL($url) {
            if ($this->_MASTER instanceof CaskeidUpnpDevice) {
            	$this->SetAVTransportURI($url);
            	return $this->Play();
            } else {
            	return false;
            }
        }

        public function SetVolume($volume, $changeByPercentFactor=0) {
            $volume = (int) $volume;
            $percent = $changeByPercentFactor;
            if ($percent) {
            	// Change the volume by percentage factor
            	foreach($this->_MEMBERS as $member) {
            	    $vol = $member->GetVolume();
            	    $vol = intval($vol * $percent);
            	    if ($vol > 100) {
            			$vol = 100;
            		}
                       $member->CallService("RenderingControl","SetVolume", array($vol));
            		}
            } else {
            	if ($volume >= 0 && $volume <= 100) {
                          foreach($this->_MEMBERS as $member) {
            			$member->CallService("RenderingControl","SetVolume", array($volume));
            		}
            	}
            }
        }
	}
}
?>
