<?
if (!class_exists("CaskeidUpnpDevice")) {
   include_once("caskeid/caskeid.class.php");
}
if (!class_exists("CaskeidSession")) {
   include_once("caskeid.session.class.php");
}

$volumeID   = 18331 ;
if ($_IPS['VARIABLE'] == $volumeID) {
   $old = GetValue($volumeID);
}
SetValue($_IPS['VARIABLE'],$_IPS['VALUE']);
$playstopID = 20596;
$playstop   = GetValue($playstopID);
$group      = "ONTHEFLY";
$gruppeID   = 31541 ;
$gruppe     = GetValue($gruppeID);
$senderID   = 14764;
$sender     = GetValue($senderID);
$volume     = GetValue($volumeID);
$session    = new CaskeidSession();

// Play-Pause ge�ndert
if ($_IPS['VARIABLE'] == $playstopID) {
	if ($playstop == 2) {
		$session->Stop();
	}
	if ($playstop == 1) {
		// Problematisch, da f�r den TV-Receiver h�here Lautst�rke erforderlich ist
		//$session->SetVolume($volume);
		$session->StartPlaybackFromURL($sender);
	}
// Lautst�rke
} else if ($_IPS['VARIABLE'] == $volumeID) {
#	if ($old > $volume) {
		$factor = (($volume * 100) / $old) / 100;
		IPS_LogMessage("CaskeRadio", "Factor: ".$factor . " from old ".$old . " to new " . $volume);
#	}
   $session->SetVolume($volume, $factor);
// Sender
} else if ($_IPS['VARIABLE'] == $senderID) {
   SetValue($playstopID, 1);
   $session->Stop();
   $session->SetVolume($volume);
	$session->StartPlaybackFromURL($sender);
// Vordefinierte Gruppe
} else if ($_IPS['VARIABLE'] == $gruppeID) {
	$groups = array(
			  -1 => "ONTHEFLY",
				0 => "ALL",
				1 => "Ergeschoss",
				2 => "KuecheWoZi",
				3 => "Kueche",
				4 => "KuecheBad"
			);
	if ($gruppe >= 0) {
      if ($playstop == 2) {
###	      file_get_contents($url . "StopGroup?group=".urlencode($group));
		}
		$bad = 25287 ;
		$finn = 21893 ;
		$kueche = 35146 ;
		$wozi = 37449 ;
		switch ($gruppe) {
			case 0:
			   SetValueBoolean($bad, true);
			   SetValueBoolean($finn, true);
			   SetValueBoolean($kueche, true);
			   SetValueBoolean($wozi, true);
			   break;
			case 1:
			   SetValueBoolean($bad, true);
			   SetValueBoolean($finn, true);
			   SetValueBoolean($kueche, true);
			   SetValueBoolean($wozi, true);
			   break;
			case 2:
				SetValueBoolean($bad, false);
			   SetValueBoolean($finn, false);
			   SetValueBoolean($kueche, true);
			   SetValueBoolean($wozi, true);
			   break;
			case 3:
				SetValueBoolean($bad, false);
			   SetValueBoolean($finn, false);
			   SetValueBoolean($kueche, true);
			   SetValueBoolean($wozi, false);
			   break;
			case 4:
				SetValueBoolean($bad, true);
			   SetValueBoolean($finn, false);
			   SetValueBoolean($kueche, true);
			   SetValueBoolean($wozi, false);
			   break;
		}
	}
}
?>
