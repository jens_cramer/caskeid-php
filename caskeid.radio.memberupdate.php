<?
// F�gt Mitglieder zur Radio-Session hinzu oder entfernt sie
//
// 28.02.15 - Reagieren auf Session-�nderungen ausserhalb des Webfronts
require_once(IPS_GetKernelDir() . '/scripts/' ."caskeid/caskeid.conf.php");

if (!class_exists("CaskeidUpnpDevice")) {
   include_once("scripts/caskeid/caskeid.class.php");
}
if (!class_exists("CaskeidSession")) {
   include_once("caskeid.session.class.php");
}

#$speakersID = CASKEID_SPEAKER_PATH;

$syncscript = 26606; // Das muss noch dynamisch werden!

$var = $_IPS['VARIABLE'];
$name = IPS_GetObject($var)['ObjectName'];

$sess = new CaskeidSession();
$status = $sess->GetMemberStatusByName($name);

if ($_IPS['VALUE']) {
	// Add
	if (!$status) {
		$sess->AddMemberByName($name);
		$sess->SetVolume( GetValue(CASKEID_RADIO_VOLUME_ID) );
		$object_id = $sess->GetMemberStatusByName($name)->GetInstance();
		create_sync_events($object_id, $name);
	}
} else {
	// Remove
	if ($status) {
		$sess->RemoveMemberByName($name);
	}
	remove_sync_events($name);
}

SetValue($_IPS['VARIABLE'],$_IPS['VALUE']);

function create_sync_events($object, $name) {
	global $syncscript;
	// Legt die zur Synchronisation mit der Session erforderlichen Events an.
	// Einen f�r die SessionID und einen f�r den Play-Status.
	$sessvar = IPS_GetObjectIDByIdent("SESSIONID",$object);
	$sessevent = IPS_CreateEvent(0);
	IPS_SetEventActive($sessevent, true);
	IPS_SetName($sessevent, "SESS_".$name);
	IPS_SetParent($sessevent, $syncscript);
	IPS_SetEventTrigger($sessevent, 1, $sessvar);
	IPS_SetIdent($sessevent, "SESS_".$name);
}

function remove_sync_events($name) {
   global $syncscript;
	// Entfernt die zur Synchronisation mit der Session erforderlichen Events.
	$sessevent = @IPS_GetObjectIDByIdent("SESS_".$name, $syncscript);
	@IPS_DeleteEvent($sessevent);
}
?>
