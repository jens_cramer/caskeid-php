<?
set_time_limit(120);
/*---------------------------------------------------------------------------/

File:
	Desc     : PHP Classes to Control Caskeid Speaker
	Version  : 0.50
	Publisher: (c)2015 Jens-Michael Cramer
	Pre-Work : (c)2015 Xaver Bauer
	Contact  : jmc@jmc.bz

/*--------------------------------------------------------------------------*/
/*##########################################################################/
/*  Class  : CaskeidUpnpDevice
/*  Desc   : Master Class to Controll Device
/*	Vars   :
/*  private _SERVICES  : (object) Holder for all Service Classes
/*  private _DEVICES   : (object) Holder for all Service Classes
/*  private _IP        : (string) IP Adress from Device
/*  private _PORT      : (int)    Port from Device
/*##########################################################################*/
if (!class_exists("CaskeidUpnpDevice") ) {

require_once("caskeid.conf.php");

class CaskeidUpnpDevice {
    private $_SERVICES=null;
    private $_DEVICES=null;
    private $_IP='';
    private $_PORT=12345;
    private $_INSTANCE = null;
    private $_UUID= null;
	
    /***************************************************************************
    /* Funktion : __construct
    /*
    /*  Benoetigt:
    /*    @url (string)  Device Url eg. '192.168.1.1:1400'
    /*
    /*  Liefert als Ergebnis: Nichts
    /*
    /****************************************************************************/
    public function __construct($url){
        $p=parse_url($url);
        $this->_IP=(isSet($p['host']))?$p['host']:$url;
        $this->_PORT=(isSet($p['port']))?$p['port']:12345;
        $this->_SERVICES=new stdClass();
        $this->_DEVICES=new stdClass();
        $this->_SERVICES->NetworkManagement=new CaskeidNetworkManagement($this);
        $this->_SERVICES->SpeakerManagement=new CaskeidSpeakerManagement($this);
        $this->_DEVICES->RenderingControl=new CaskeidRenderingControl($this);
        $this->_DEVICES->ConnectionManager=new CaskeidConnectionManager($this);
        $this->_DEVICES->AVTransport=new CaskeidAVTransport($this);
        $this->_SERVICES->SessionManagement=new CaskeidSessionManagement($this);
    }

	public function GetInstance() {
			if ($this->_INSTANCE) {
				return $this->_INSTANCE;
			} else {
                $this->_INSTANCE = CaskeidUpnpDevice::getInstanceIdFromIP($this->_IP);
                return $this->_INSTANCE;
			}
	}
	
    public static function getDeviceFolderID() {
        return CASKEID_SPEAKER_PATH;
    }
    
	public static function getIdentFromIP($ip) {
		return "CASKE" . str_replace(".","",$ip);
	}
	
	public static function getInstanceByName($name) {
        $speakerID = IPS_GetObjectIDByName($name,CaskeidUpnpDevice::getDeviceFolderID());
        if (!$speakerID) {
			return null;
		}
        $ip   = GetValue(IPS_GetObjectIDByIdent("IP",$speakerID));
        $port = GetValue(IPS_GetObjectIDByIdent("PORT",$speakerID));
        return new CaskeidUpnpDevice("http://".$ip.":".$port);
	}
	
	public static function getInstanceIdFromIP($ip) {
        foreach(CaskeidUpnpDevice::getDeviceArray() as $s) {
    		if ($s == $ip) {
    			$speakerID = IPS_GetObjectIDByIdent(CaskeidUpnpDevice::getIdentFromIP($s),CaskeidUpnpDevice::getDeviceFolderID());
    			return $speakerID;
    		}
		}
	}
	
	/**
	 * Dynamically updates the array of known devices
	 */
	public static function getDeviceArray() {
	    $out = array();
	    foreach(IPS_GetChildrenIDs(CaskeidUpnpDevice::getDeviceFolderID()) as $child) {
    		$ip   = GetValue(IPS_GetObjectIDByIdent("IP",$child));
    		$name = GetValue(IPS_GetObjectIDByIdent("NAME",$child));
    		$out[$name] = $ip;
	    }
	    return $out;
	}
	
	/**
	 * Finds all Caskeid-Devices in your network and creates a device in your devicefolder
	 */ 
	public static function findAndCreateDevices() {
	    $res = CaskeidUpnpDevice::mSearch("urn:schemas-pure-com:service:RTSPGateway:1");
	    echo("Habe " . sizeof($res) . " Treffer gefunden.\n");

	    foreach($res as $d) {
		// USN
		$usn  = explode("::", $d['usn']);
		$usn  = $usn[0];
		// IP and Port
		$url  = parse_url($d['location']);
		$ip   = $url['host'];
		$port = $url['port'];
		// Create Device if not existent
		$device_found = false;
		foreach(IPS_GetChildrenIDs(CaskeidUpnpDevice::getDeviceFolderID()) as $child) {
		    $uidID = IPS_GetObjectIDByIdent("UUID",$child);
		    if ($uidID && GetValue($uidID) == $usn) {
			$device_found = true;
            echo("Device " . $usn . " already exists.");
			break;
		    }
		}
		if(!$device_found) {
		    CaskeidUpnpDevice::createDevice($usn, $ip, $usn);
		}
	    }
	    
	    // Update Ports to get device name and unlock multi-speaker functionality
	    CaskeidUpnpDevice::updatePorts();
	}
	
	public static function updatePorts() {
			// Updates the used IP-Port of our speakers
			$res = CaskeidUpnpDevice::mSearch("urn:schemas-pure-com:service:RTSPGateway:1");

			echo("Habe " . sizeof($res) . " Treffer gefunden.\n");

			foreach($res as $d) {
				$url = parse_url($d['location']);
				foreach(CaskeidUpnpDevice::getDeviceArray() as $s) {
					if ($s == $url['host']) {
						echo($s . " hört auf Port " . $url['port']."\n");
						$speakerID = IPS_GetObjectIDByIdent(CaskeidUpnpDevice::getIdentFromIP($s),CaskeidUpnpDevice::getDeviceFolderID());
						$portID = IPS_GetObjectIDByIdent("PORT",$speakerID);
						if (GetValue($portID) != $url['port']) {
							SetValue($portID, $url['port']);
						}
						// Um die Caskeid-Lautsprecher "freizuschalten" (d.h. eine Steuerung zu ermöglichen) müssen
						// einmal die folgenden URLs per HTTP abgerufen werden:
						//
						// /config.lp, /portal_registration.lp and /xml/SpeakerManagement.xml
						//
						// Tut man dies nicht mind. einmal nach dem Start der Box geht es nicht im Multi-Speaker-Betrieb.
						$opts = array('http' =>
							array(
							    'timeout' => 2,
							    'header'=> "User-Agent: Nemo Android Application"
						  	)
						);
						$context  = stream_context_create($opts);
						$myurl = 'http://'.$s."/cgi-bin/config.lp?output=json";
						$config = @json_decode(file_get_contents($myurl, false, $context));
						$instance = CaskeidUpnpDevice::getInstanceIdFromIP($s);
						// Wir bekommen mit der config.lp auch den Device-Namen. Diesen
						// setzen wir auch in der Instanz von IPS, falls der User den
						// über des Webfront des Lautsprechers geändert haben sollte.
						if ($config->device_name) {
							IPS_SetName($instance , $config->device_name);
							SetValueString( IPS_GetObjectIDByIdent("NAME", $instance), $config->device_name );
						}
						
						$opts = array('http' =>
							array(
							    'timeout' => 2,
							    'header'=> "User-Agent: Linux/3.4.0-cyanogenmod-gb554143 UPnP/1.1 PureLibUPnP/0.1"
						  	)
						);
						$context  = stream_context_create($opts);
						$myurl = 'http://'.$s."/cgi-bin/portal_registration.lp?action=get&output=json";
						@file_get_contents($myurl, false, $context);

						$opts = array('http' =>
							array(
							    'timeout' => 2,
							    'header'=> "User-Agent: Dalvik/1.6.0 (Linux; U; Android 4.4.4; Nexus 5 Build/KTU84P)"
						  	)
						);
						$context  = stream_context_create($opts);
						$myurl = 'http://'.$s.":".$url['port']."/xml/SpeakerManagement.xml";
						@file_get_contents($myurl, false, $context);
					}
				}
			}
			if (sizeof($res) < CaskeidUpnpDevice::getDeviceArray()) {
				return false;
			} else {
				return true;
			}
		}

      private static function mSearch( $st = 'ssdp:all', $mx = 2, $man = 'ssdp:discover', $from = null, $port = null, $sockTimout = '10' ) {
		$USER_AGENT = 'MacOSX/10.8.2 UPnP/1.1 PHP-UPnP/0.0.1a';
		// BUILD MESSAGE
		$msg  = 'M-SEARCH * HTTP/1.1' . "\r\n";
		$msg .= 'HOST: 239.255.255.250:1900' ."\r\n";
		$msg .= 'MAN: "'. $man .'"' . "\r\n";
		$msg .= 'MX: '. $mx ."\r\n";
		$msg .= 'ST:' . $st ."\r\n";
		$msg .= 'USER-AGENT: '. $USER_AGENT ."\r\n";
		$msg .= '' ."\r\n";

		// MULTICAST MESSAGE
		$sock = socket_create( AF_INET, SOCK_DGRAM, 0 );
		$opt_ret = @socket_set_option( $sock, 1, 6, TRUE );
		$send_ret = socket_sendto( $sock, $msg, strlen( $msg ), 0, '239.255.255.250', 1900);

		// SET TIMEOUT FOR RECIEVE
		@socket_set_option( $sock, SOL_SOCKET, SO_RCVTIMEO, array( 'sec'=>$sockTimout, 'usec'=>'0' ) );

		// RECIEVE RESPONSE
		$response = array();
		do {
			$buf = null;
			@socket_recvfrom( $sock, $buf, 1024, 0, $from, $port );
			if( !is_null($buf) )$response[] = CaskeidUpnpDevice::parseMSearchResponse( $buf );
		} while( !is_null($buf) );

		// CLOSE SOCKET
		socket_close( $sock );

		return $response;
	}

	private static function parseMSearchResponse( $response )	{
		$responseArr = explode( "\r\n", $response );

		$parsedResponse = array();

		foreach( $responseArr as $row ) {
			if( stripos( $row, 'http' ) === 0 )
					$parsedResponse['http'] = $row;

			if( stripos( $row, 'cach' ) === 0 )
					$parsedResponse['cache-control'] = str_ireplace( 'cache-control: ', '', $row );

			if( stripos( $row, 'date') === 0 )
					$parsedResponse['date'] = str_ireplace( 'date: ', '', $row );

			if( stripos( $row, 'ext') === 0 )
					$parsedResponse['ext'] = str_ireplace( 'ext: ', '', $row );

			if( stripos( $row, 'loca') === 0 )
					$parsedResponse['location'] = str_ireplace( 'location: ', '', $row );

			if( stripos( $row, 'serv') === 0 )
					$parsedResponse['server'] = str_ireplace( 'server: ', '', $row );

			if( stripos( $row, 'st:') === 0 )
					$parsedResponse['st'] = str_ireplace( 'st: ', '', $row );

			if( stripos( $row, 'usn:') === 0 )
					$parsedResponse['usn'] = str_ireplace( 'usn: ', '', $row );

			if( stripos( $row, 'cont') === 0 )
					$parsedResponse['content-length'] = str_ireplace( 'content-length: ', '', $row );
		}

		return $parsedResponse;
	}
	
	public static function createDevice($name, $ip, $uid) {
		$InstanceId = IPS_CreateInstance("{485D0419-BE97-4548-AA9C-C083EB82E61E}");
		IPS_SetParent($InstanceId, CaskeidUpnpDevice::getDeviceFolderID());
		IPS_SetName($InstanceId, $name);
		IPS_SetIdent($InstanceId, CaskeidUpnpDevice::getIdentFromIP($ip));
		IPS_SetPosition($InstanceId, 0);
		IPS_ApplyChanges($InstanceId);

		$NameId = IPS_CreateVariable(3);
		IPS_SetParent($NameId, $InstanceId);
		IPS_SetName($NameId, "Name");
		IPS_SetIdent($NameId, "NAME");
		IPS_SetPosition($NameId, 0);
		SetValue($NameId, $name);
		IPS_SetHidden($NameId, true);

		$ipID = IPS_CreateVariable(3);
		IPS_SetParent($ipID, $InstanceId);
		IPS_SetName($ipID, "IP");
		IPS_SetIdent($ipID, "IP");
		IPS_SetPosition($ipID, 0);
		SetValue($ipID, $ip);
		IPS_SetHidden($ipID, true);
		
		$portID = IPS_CreateVariable(3);
		IPS_SetParent($portID, $InstanceId);
		IPS_SetName($portID, "Port");
		IPS_SetIdent($portID, "PORT");
		IPS_SetPosition($portID, 0);
		SetValue($portID, "0");
		IPS_SetHidden($portID, true);

		$playstateID = IPS_CreateVariable(3);
		IPS_SetParent($playstateID, $InstanceId);
		IPS_SetName($playstateID, "PlayState");
		IPS_SetIdent($playstateID, "PLAYSTATE");
		IPS_SetPosition($playstateID, 70);
		SetValue($playstateID, "STOPPED");

  		$btcaskeid = IPS_CreateVariable(0);
		IPS_SetParent($btcaskeid, $InstanceId);
		IPS_SetName($btcaskeid, "BluetoothCaskeid");
		IPS_SetIdent($btcaskeid, "BTCASKEID");
		IPS_SetPosition($btcaskeid, 100);
		IPS_SetVariableCustomProfile($btcaskeid, "~Switch");
		IPS_SetVariableCustomAction($btcaskeid, CASKEID_ACTIONSCRIPT);
		SetValue($btcaskeid, false);

      $sessionMaster = IPS_CreateVariable(0);
		IPS_SetParent($sessionMaster, $InstanceId);
		IPS_SetName($sessionMaster, "SessionMaster");
		IPS_SetIdent($sessionMaster, "SESSIONMASTER");
		IPS_SetPosition($sessionMaster, 510);
		SetValue($sessionMaster, false);

		$trackuri = IPS_CreateVariable(3);
		IPS_SetParent($trackuri, $InstanceId);
		IPS_SetName($trackuri, "CurrentTrackURI");
		IPS_SetIdent($trackuri, "CURRENTTRACKURI");
		IPS_SetPosition($trackuri, 200);
		SetValue($trackuri, "");
		
		$sessionid = IPS_CreateVariable(3);
		IPS_SetParent($sessionid, $InstanceId);
		IPS_SetName($sessionid, "SessionID");
		IPS_SetIdent($sessionid, "SESSIONID");
		IPS_SetPosition($sessionid, 500);
		SetValue($sessionid, "");
		
		$uuid = IPS_CreateVariable(3);
		IPS_SetParent($uuid, $InstanceId);
		IPS_SetName($uuid, "UUID");
		IPS_SetIdent($uuid, "UUID");
		IPS_SetPosition($uuid, 0);
		SetValue($uuid, $uid);
		IPS_SetHidden($uuid, true);

		$volumeID = IPS_CreateVariable(1);
		IPS_SetParent($volumeID, $InstanceId);
		IPS_SetName($volumeID, "Volume");
		IPS_SetIdent($volumeID, "VOLUME");
		IPS_SetPosition($volumeID, 0);
		SetValue($volumeID, 80);
		IPS_SetVariableCustomProfile($volumeID, "~Intensity.100");
		IPS_SetVariableCustomAction($volumeID, CASKEID_ACTIONSCRIPT);
	}
	
	public function GetUUID() {
			if ($this->_UUID) {
				return $this->_UUID;
			} else {
            $this->_UUID = GetValue(IPS_GetObjectIDByIdent("UUID", $this->GetInstance()));
            return $this->_UUID;
			}
	}
	
	public function PrintFriendlyDescriptor($onlyReturnXML) {
		$uuid = substr($this->GetUUID(),5);
		$xml = file_get_contents('http://'.$this->_IP.":".$this->_PORT."/".$uuid.".xml");
		if ($onlyReturnXML) {
			return $xml;
		} else {
         print_r(simplexml_load_string($xml));
		}
	}
	
    /***************************************************************************
    /* Funktion : GetIcon
    /*
    /*  Benoetigt:
    /*    @IconNr (int)
    /*
    /*  Liefert als Ergebnis: Array mit folgenden Keys
    /*    @width  (int)
    /*    @height (int)
    /*    @url (string)
    /*
    /****************************************************************************/
    function GetIcon($id) {
        switch($id){
            case 0 : return array('width'=>48,'height'=>48,'url'=>'http://'.$this->_IP.":".$this->_PORT.'/org.mpris.MediaPlayer2.mansion-48x48x32.png');break;
        }
        return array('width'=>0,'height'=>0,'url'=>'');
    }
    /***************************************************************************
    /* Funktion : IconCount
    /*
    /*  Benoetigt: Nichts
    /*
    /*  Liefert als Ergebnis:
    /*    @count (int) => The Numbers of Icons Avail
    /*
    /****************************************************************************/
    function IconCount() { return 1;}
    /***************************************************************************
    /* Funktion : Upnp
    /*
    /*  Benoetigt:
    /*    @url (string)
    /*    @SOAP_service (string)
    /*    @SOAP_action (string)
    /*    @SOAP_arguments (sting) [Optional]
    /*    @XML_filter (string|stringlist|array of strings) [Optional]
    /*
    /*  Liefert als Ergebnis:
    /*    @result (string|array) => The XML Soap Result
    /*
    /****************************************************************************/
    public function Upnp($url,$SOAP_service,$SOAP_action,$SOAP_arguments = '',$XML_filter = ''){
        $POST_xml = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">';
        $POST_xml .= '<s:Body>';
        $POST_xml .= '<u:'.$SOAP_action.' xmlns:u="'.$SOAP_service.'">';
        $POST_xml .= $SOAP_arguments;
        $POST_xml .= '</u:'.$SOAP_action.'>';
        $POST_xml .= '</s:Body>';
        $POST_xml .= '</s:Envelope>';
        $POST_url = $this->_IP.":".$this->_PORT.$url;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_URL, $POST_url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/xml", "SOAPAction: ".$SOAP_service."#".$SOAP_action));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $POST_xml);
        $r = curl_exec($ch);
        if (curl_errno($ch)) {
				IPS_LogMessage("Curl error",curl_error($ch));
		  }
        curl_close($ch);
        if ($XML_filter != '')
            return $this->Filter($r,$XML_filter);
        else
            return $r;
    }
    /***************************************************************************
    /* Funktion : Filter
    /*
    /*  Benoetigt:
    /*    @subject (string)
    /*    @pattern (string|stringlist|array of strings)
    /*
    /*  Liefert als Ergebnis:
    /*    @result (array|variant) => Array format FilterPattern=>Value
    /*
    /****************************************************************************/
    public function Filter($subject,$pattern){
        $multi=is_array($pattern);
        if(!$multi){
            $pattern=explode(',',$pattern);
            $multi=(count($pattern)>1);
        }
        #IPS_LogMessage("Filter Pattern", print_r($pattern, true));
        foreach($pattern as $pat){
            if(!$pat)continue;
            preg_match('/\<'.$pat.'\>(.+)\<\/'.$pat.'\>/',$subject,$matches);
            if($multi)$n[$pat]=(isSet($matches[1]))?$matches[1]:false;
            else return (isSet($matches[1]))?$matches[1]:false;
        }
        return $n;
    }
    /***************************************************************************
    /* Funktion : GetServiceNames
    /*
    /*  Benoetigt: Nichts
    /*
    /*  Liefert als Ergebnis:
    /*    @result (array) => Namen der vorhandenen Services
    /*
    /****************************************************************************/
    public function GetServiceNames(){
        foreach($this->_SERVICES as $fn=>$tmp)if(substr($fn,0,1)!='_')$n[]=$fn;
        foreach($this->_DEVICES as $fn=>$tmp)if(substr($fn,0,1)!='_')$n[]=$fn;
        return $n;
    }
    /***************************************************************************
    /* Funktion : GetServiceFunctionNames
    /*
    /*  Benoetigt:
    /*    @ServiceName (string)
    /*
    /*  Liefert als Ergebnis:
    /*    @result (array) => Namen der vorhandenen Service Funktionen
    /*
    /****************************************************************************/
    public function GetServiceFunctionNames($ServiceName){
        if(isSet($this->_SERVICES->$ServiceName)){
            $p=&$this->_SERVICES->$ServiceName;
        }else if(isSet($this->_DEVICES->$ServiceName)){
            $p=&$this->_DEVICES->$ServiceName;
        }else throw new Exception('Unbekanner Service-Name '.$ServiceName.' !!!');
        foreach(get_class_methods($p) as $fn)if(substr($fn,0,1)!='_')$n[]=$fn;
        return $n;
    }
    /***************************************************************************
    /* Funktion : CallService
    /*
    /*  Benoetigt:
    /*    @ServiceName (string)
    /*    @FunctionName (string)
    /*    @Arguments (string,array) [Optional] Funktions Parameter
    /*
    /*  Liefert als Ergebnis:
    /*    @result (array|variant) => siehe Funktion
    /*
    /****************************************************************************/
    public function CallService($ServiceName, $FunctionName, $Arguments=null){
        if(is_object($ServiceName))$p=$ServiceName;
        else if(isSet($this->_SERVICES->$ServiceName)){
            $p=&$this->_SERVICES->$ServiceName;
        }else if(isSet($this->_DEVICES->$ServiceName)){
            $p=&$this->_DEVICES->$ServiceName;
        }else throw new Exception('Unbekanner Service-Name '.$ServiceName.' !!!');
        if(!method_exists($p,$FunctionName)) throw new Exception('Unbekannter Funktions-Name '.$FunctionName.' !!! Service:'.$ServiceName);
        if(!is_null($Arguments)){
            $a=&$Arguments;
            if (!is_array($a))$a=Array($a);
				#print_r($a);
            switch(count($a)){
                case 1: return $p->$FunctionName($a[0]);break;
                case 2: return $p->$FunctionName($a[0],$a[1]);break;
                case 3: return $p->$FunctionName($a[0],$a[1],$a[2]);break;
                case 4: return $p->$FunctionName($a[0],$a[1],$a[2],$a[3]);break;
                case 5: return $p->$FunctionName($a[0],$a[1],$a[2],$a[3],$a[4]);break;
                case 6: return $p->$FunctionName($a[0],$a[1],$a[2],$a[3],$a[4],$a[5]);break;
                case 7: return $p->$FunctionName($a[0],$a[1],$a[2],$a[3],$a[4],$a[5],$a[6]);break;
                case 8: return $p->$FunctionName($a[0],$a[1],$a[2],$a[3],$a[4],$a[5],$a[6],$a[7]);break;
                case 9: return $p->$FunctionName($a[0],$a[1],$a[2],$a[3],$a[4],$a[5],$a[6],$a[7],$a[8]);break;
                case 10: return $p->$FunctionName($a[0],$a[1],$a[2],$a[3],$a[4],$a[5],$a[6],$a[7],$a[8],$a[9]);break;
                case 11: return $p->$FunctionName($a[0],$a[1],$a[2],$a[3],$a[4],$a[5],$a[6],$a[7],$a[8],$a[9],$a[10]);break;
                case 12: return $p->$FunctionName($a[0],$a[1],$a[2],$a[3],$a[4],$a[5],$a[6],$a[7],$a[8],$a[9],$a[10],$a[11]);break;
                default: return $p->$FunctionName();
            }
        }else return $p->$FunctionName();
    }
    /***************************************************************************
    /* Funktion : __call
    /*
    /*  Benoetigt:
    /*    @FunctionName (string)
    /*    @arguments (array)
    /*
    /*  Liefert als Ergebnis:
    /*    @result (variant) => siehe aufzurufende Funktion
    /*
    /****************************************************************************/
    public function __call($FunctionName, $arguments){
        if(!$p=$this->_ServiceObjectByFunctionName($FunctionName))
            throw new Exception('Unbekannte Funktion '.$FunctionName.' !!!');
        return $this->CallService($p,$FunctionName, $arguments);
    }
    /***************************************************************************
    /* Funktion : _ServiceObjectByFunctionName
    /*
    /*  Benoetigt:
    /*    @FunctionName (string)
    /*
    /*  Liefert als Ergebnis:
    /*    @result (function||null) ServiceObject mit der gusuchten Function
    /*
    /****************************************************************************/
    private function _ServiceObjectByFunctionName($FunctionName){
        foreach($this->_SERVICES as $fn=>$tmp)if(method_exists($this->_SERVICES->$fn,$FunctionName)){return $this->_SERVICES->$fn;}
        foreach($this->_DEVICES as $fn=>$tmp)if(method_exists($this->_DEVICES->$fn,$FunctionName)){return $this->_DEVICES->$fn;}
        return false;
    }
    /***************************************************************************
    /* Funktion : sendPacket
    /*
    /*  Benoetigt:
    /*    @content (string)
    /*
    /*  Liefert als Ergebnis:
    /*    @result (array)
    /*
    /****************************************************************************/
    public function sendPacket( $content ){
      	#echo $content;
        $fp = fsockopen($this->_IP, $this->_PORT, $errno, $errstr, 10);
        if (!$fp) {
		  		throw new Exception("Error opening socket: ".$errstr." (".$errno.")");
		  }
        stream_set_timeout($fp , 1); // Irgendwie sendet Caskeid kein erkennbares Daten-Ende für PHP. Mit diesem Timeout geht es aber
            fputs ($fp,$content);
				$ret = "";
				$time = time();
            while (!feof($fp)){
               if (time() - $time > 5) {
						break;
					}
					$ret.= fgetss($fp); // filters xml answer
				} 
            fclose($fp);
        if(strpos($ret, "200 OK") === false)throw new Exception("Error sending command: ".$ret);
        foreach(preg_split("/\n/", $ret) as $v)if(trim($v)&&(strpos($v,"200 OK")===false))$array[]=trim($v);
        return $array;
    }
    /***************************************************************************
    /* Funktion : GetBaseUrl
    /*
    /*  Benoetigt: Nichts
    /*
    /*  Liefert als Ergebnis:
    /*    @result (string)
    /*
    /****************************************************************************/
    public function GetBaseUrl(){
        return $this->_IP.':'.$this->_PORT;
    }
    
    public function GetIP() {
			return $this->_IP;
	 }
	 
	 public function GetVolume() {
			return GetValueInteger(IPS_GetObjectIDByIdent("VOLUME", $this->GetInstance()));
	 }
	 
	 public function SetSessionID($sess) {
         return SetValueString(IPS_GetObjectIDByIdent("SESSIONID", $this->GetInstance()), $sess);
	 }
	 
	 public function getSessionID() {
         return GetValueString(IPS_GetObjectIDByIdent("SESSIONID", $this->GetInstance()));
	 }
	 
	 public function GetName() {
         return GetValueString(IPS_GetObjectIDByIdent("NAME", $this->GetInstance()));
	 }
	 
	 public function SetIsSessionMaster($value) {
         return SetValueBoolean(IPS_GetObjectIDByIdent("SESSIONMASTER", $this->GetInstance()), $value);
	 }
	 
	 public function GetIsSessionMaster() {
         return GetValueBoolean(IPS_GetObjectIDByIdent("SESSIONMASTER", $this->GetInstance()));
	 }
	 
	 public function GetPlayState() {
			return GetValueString(IPS_GetObjectIDByIdent("PLAYSTATE", $this->GetInstance()));
	 }
	 
	 public function GetCurrentTrackURI() {
         return GetValueString(IPS_GetObjectIDByIdent("CURRENTTRACKURI", $this->GetInstance()));
	 }
	 
	 public function CreateSession() {
			if (!$this->GetSessionID()) {
			   $this->CallService("ConnectionManager","PrepareForConnection", array());
			   $id = $this->Filter($this->CallService("SessionManagement","CreateSession", array("")),"SessionID");
            $this->SetSessionID( $id );
            $this->SetIsSessionMaster(true);
			}
			IPS_LogMessage("Caskeid Session", "Created Session " . $this->getSessionID() . " with Master " . $this->GetName());
			return $this->getSessionID();
	 }
	 
	 public function AddUnitToSession($UUID){
			if ($this->GetIsSessionMaster()) {
			   $uuid = substr($UUID, 5);
            $this->CallService("SessionManagement","AddUnitToSession", array( $this->getSessionID(), $uuid));
			}
			IPS_LogMessage("Caskeid Session", "Added " . $UUID . " / to / " . $this->getSessionID());
	 }
	 
	 public function RemoveUnitFromSession($UUID){
			if ($this->GetIsSessionMaster()) {
			   $uuid = substr($UUID, 5);
            $this->CallService("SessionManagement","RemoveUnitFromSession", array( $this->getSessionID(), $uuid));
			}
			IPS_LogMessage("Caskeid Session", "Removed " . $uuid . "  / from / " . $this->getSessionID());
	 }
	 
	 public function PlayMessage($transportUrl, $volume) {
			// Wenn gerade etwas wiedergegeben wird speichern wir die URL um die
			// Wiedergabe anschliessend fortzusetzen.
			$wasPlayingSomething = $this->GetPlayState() == "STOPPED" ? false : true;
			
			if ($wasPlayingSomething) {
			   $oldURI = $this->GetCurrentTrackURI();
			   # Aktuelle Position speichern
			   $posinfo = $this->CallService("AVTransport","GetPositionInfo", array());
			   $position = $posinfo['RelTime'];
			   $duration = $posinfo['TrackDuration'];
			   # Stoppen
            $this->CallService("AVTransport","SyncStop", array());
            IPS_Sleep(500);
			}
			// Aktuelle Lautstärke sichern um sie ggf. wiederherstellen zu können
			$oldVol = $this->GetVolume();
			if ($volume != $oldVol) {
            $this->CallService("RenderingControl","SetVolume", array($volume));
            IPS_Sleep(500);
			}
			$this->CallService("AVTransport","SetAVTransportURI", array($transportUrl));
			$this->CallService("ConnectionManager","PrepareForConnection", array());
			$this->CallService("AVTransport","SyncPlay", array());
			//
			if ($volume != $oldVol) {
				// Warten das die Wiedergabe beginnt
			   IPS_Sleep(3000);
			   // Das ändern der Lautstärke tritt sofort in Kraft. Deshalb muss
			   // gewartet werden bis die Wiedergabe beendet ist, bevor die Lautstärke
			   // zurück gesetzt wird.
			   $start = time();
			   while($this->GetPlayState() == "PLAYING" OR $this->GetPlayState == "TRANSITIONING") {
     				IPS_Sleep(1000);
					if ( (time() - $start) > 15 ) {
						// Nach max. 15 Sekunden Abbruch - damit es keine Endlosschleife wird.
						// Länger sollte auch keine Ansage sein.
						break;
					}
				}
            $this->CallService("RenderingControl","SetVolume", array($oldVol));
			}
			// Wiedergabe ggf. wiederherstellen
			if ($wasPlayingSomething) {
			   IPS_Sleep(1000);
            $this->CallService("AVTransport","SetAVTransportURI", array($oldURI));
				$this->CallService("ConnectionManager","PrepareForConnection", array());
				$this->CallService("AVTransport","SyncPlay", array());
				if ($duration != "0:00:00") {
               IPS_Sleep(3000);
               $this->CallService("AVTransport","Seek", array($position));
				}
			}
	 }
}
}
/*##########################################################################/
/*  Class  : CaskeidUpnpClass
/*  Desc   : Basis Class for Services
/*	Vars   :
/*  private SERVICE     : (string) Service URN
/*  private SERVICEURL  : (string) Path to Service Control
/*  private EVENTURL    : (string) Path to Event Control
/*  public  BASE        : (Object) Points to MasterClass
/*##########################################################################*/
if (!class_exists("CaskeidUpnpClass") ) {
class CaskeidUpnpClass {
    protected $SERVICE="";
    protected $SERVICEURL="";
    protected $EVENTURL="";
    var $BASE=null;
    /***************************************************************************
    /* Funktion : __construct
    /*
    /*  Benoetigt:
    /*    @BASE (object) Referenz of MasterClass
    /*    @SERVICE (string) [Optional]
    /*    @SERVICEURL (string) [Optional]
    /*    @EVENTURL (string) [Optional]
    /*
    /*  Liefert als Ergebnis: Nichts
    /*
    /****************************************************************************/
    public function __construct($BASE, $SERVICE="", $SERVICEURL="", $EVENTURL=""){
        $this->BASE=$BASE;
        if($SERVICE)$this->SERVICE=$SERVICE;
        if($SERVICEURL)$this->SERVICEURL=$SERVICEURL;
        if($EVENTURL)$this->EVENTURL=$EVENTURL;
    }
    /***************************************************************************
    /* Funktion : RegisterEventCallback
    /*
    /*  Benoetigt:
    /*    @callback_url (string) Url die bei Ereignissen aufgerufen wird
    /*    @timeout (int) Gueltigkeitsdauer der CallbackUrl
    /*
    /*  Liefert als Ergebnis: Array mit folgenden Keys
    /*    @SID (string)
    /*    @TIMEOUT (int)
    /*    @Server (string)
    /*
    /****************************************************************************/
    public function RegisterEventCallback($callback_url,$timeout=300){
        if(!$this->EVENTURL)return false;
        $content='SUBSCRIBE '.$this->EVENTURL.' HTTP/1.1
HOST: '.$this->BASE->GetBaseUrl().'
CALLBACK: <'.$callback_url.'>
NT: upnp:event
TIMEOUT: Second-'.$timeout.'

';//Content-Length: 0

        $a=$this->BASE->sendPacket($content);
		  $res=false;
        #print_r( $a);
        if($a)foreach($a as $r){$m=explode(':',$r);if(isSet($m[1])){$b=array_shift($m);$res[$b]=implode(':',$m);}}
        return $res;
    }
    /***************************************************************************
    /* Funktion : UnRegisterEventCallback
    /*
    /*  Benoetigt:
    /*    ?
    /*
    /*  Liefert als Ergebnis:
    /*    ?
    /*
    /****************************************************************************/
    public function UnRegisterEventCallback(){
        if(!$this->EVENTURL)return false;
        $content='UNSUBSCRIBE '.$this->EVENTURL.' HTTP/1.1
HOST: '.$this->BASE->GetBaseUrl().'
Content-Length: 0

';
        return $this->BASE->sendPacket($content);
    }
}
}
/*##########################################################################*/
/*  Class  : MusicServices
/*  Service: urn:schemas-upnp-org:service:MusicServices:1
/*	     Id: urn:upnp-org:serviceId:MusicServices
/*##########################################################################*/
if (!class_exists("CaskeidNetworkManagement") ) {
	class CaskeidNetworkManagement extends CaskeidUpnpClass {
	    protected $SERVICE='urn:schemas-pure-com:service:NetworkManagement:1';
	    protected $SERVICEURL='/Control/org.mpris.MediaPlayer2.mansion/RygelNetworkManagement';
	    protected $EVENTURL='Event/org.mpris.MediaPlayer2.mansion/RygelNetworkManagement';
	    /***************************************************************************
	    /* Funktion : GetNetworkStatus
	    /*
	    /*  Benoetigt: nix
	    /*
	    /*  Liefert als Ergebnis:
	    /*          @CurrentNetworkStatus (string)
	    /*
	    /****************************************************************************/
	    public function GetNetworkStatus(){
	        $args="";
	        $filter="networkStatus";
	        return $this->BASE->Upnp($this->SERVICEURL,$this->SERVICE,'GetNetworkStatus',$args,$filter);
	    }}
}



/*##########################################################################*/
/*  Class  : GroupManagement
/*  Service: urn:schemas-upnp-org:service:GroupManagement:1
/*	     Id: urn:upnp-org:serviceId:GroupManagement
/*##########################################################################*/
if (!class_exists("CaskeidSpeakerManagement") ) {
	class CaskeidSpeakerManagement extends CaskeidUpnpClass {
	    protected $SERVICE='urn:schemas-pure-com:service:SpeakerManagement:1';
	    protected $SERVICEURL='/Control/org.mpris.MediaPlayer2.mansion/RygelSpeakerManagement';
	    protected $EVENTURL='/Event/org.mpris.MediaPlayer2.mansion/RygelSpeakerManagement';

	    public function HandleEvent($xml) {
	         $instance = CaskeidUpnpDevice::getInstanceIdFromIP($this->BASE->GetIP());
	         $xml = $this->BASE->filter($xml, "Groups,SessionID");
	         $groups = htmlspecialchars_decode($xml['Groups']);
	         $session = $xml['SessionID'];
	         if(strpos($groups, "4DAA44")) {
	         	SetValueBoolean(IPS_GetObjectIDByIdent("BTCASKEID",$instance), true);
				} else {
					SetValueBoolean(IPS_GetObjectIDByIdent("BTCASKEID",$instance), false);
				}
				if ($session) {
					// Ist in Session
					SetValue(IPS_GetObjectIDByIdent("SESSIONID",$instance), $session);
					// Check ob GetSession einen Wert liefert. Dies tut es nur beim SessionMaster
					//IPS_LogMessage("SessionMaster", $this->GetSession());
				} else {
					// Keine Session
					SetValue(IPS_GetObjectIDByIdent("SESSIONID",$instance), "");
					SetValue(IPS_GetObjectIDByIdent("SESSIONMASTER",$instance), false);
				}
				//IPS_LogMessage("SpeakerMan Handle", print_r($xml,true));
		 }

	    public function GetGroups() {
	    		$args="";
	        	$filter="CurrentGroups";
				return $this->BASE->Upnp($this->SERVICEURL,$this->SERVICE,'GetGroups',$args,$filter);
		 }


		 public function EnableBTCaskeid($enable) {
		      $id = "4DAA44C0-8291-11E3-BAA7-0800200C9A66";
				if ($enable) {
					return $this->AddToGroup($id,"Bluetooth");
				} else {
					return $this->RemoveFromGroup($id);
				}
		 }

	    public function AddToGroup($ID, $NAME){
	        $args="<ID>$ID</ID><Name>$NAME</Name><Metadata></Metadata>";
	        IPS_LogMessage("AddToGroup",$args);
	        $filter="";
	        return $this->BASE->Upnp($this->SERVICEURL,$this->SERVICE,'AddToGroup',$args,$filter);
	    }
	    
	    public function ClearSession($ID){
	        $args="<SessionID>$ID</SessionID>";
	        IPS_LogMessage("ClearSession",$args);
	        $filter="";
	        return $this->BASE->Upnp($this->SERVICEURL,$this->SERVICE,'ClearSession',$args,$filter);
	    }

	    public function RemoveFromGroup($ID){
	        $args="<ID>$ID</ID>";
	        $filter="";
	        return $this->BASE->Upnp($this->SERVICEURL,$this->SERVICE,'RemoveFromGroup',$args,$filter);
	    }}
}
/*##########################################################################*/
/*  Class  : ConnectionManager
/*  Service: urn:schemas-upnp-org:service:ConnectionManager:1
/*	     Id: urn:upnp-org:serviceId:ConnectionManager
/*##########################################################################*/
if (!class_exists("CaskeidConnectionManager") ) {
	class CaskeidConnectionManager extends CaskeidUpnpClass {
	    protected $SERVICE='urn:schemas-upnp-org:service:ConnectionManager:1';
	    protected $SERVICEURL='/MediaServer/ConnectionManager/Control';
	    protected $EVENTURL='/Event/org.mpris.MediaPlayer2.mansion/RygelSinkConnectionManager';

	    public function PrepareForConnection() {
				$args = "<PeerConnectionID>0</PeerConnectionID><RemoteProtocolInfo></RemoteProtocolInfo><PeerConnectionManager></PeerConnectionManager>";
				$args .= "<Direction>INPUT</Direction>";
				$filter = "ConnectionID";
				return $this->BASE->Upnp($this->SERVICEURL,$this->SERVICE,'PrepareForConnection',$args,$filter);
		 }
	    /***************************************************************************
	    /* Funktion : GetProtocolInfo
	    /*
	    /*  Benoetigt: Nichts
	    /*
	    /*  Liefert als Ergebnis: Array mit folgenden Keys
	    /*          @Source (string)
	    /*          @Sink (string)
	    /*
	    /****************************************************************************/
	    public function GetProtocolInfo(){
	        $args="";
	        $filter="Source,Sink";
	        return $this->BASE->Upnp($this->SERVICEURL,$this->SERVICE,'GetProtocolInfo',$args,$filter);
	    }
	    /***************************************************************************
	    /* Funktion : GetCurrentConnectionIDs
	    /*
	    /*  Benoetigt: Nichts
	    /*
	    /*  Liefert als Ergebnis:
	    /*          @ConnectionIDs (string)
	    /*
	    /****************************************************************************/
	    public function GetCurrentConnectionIDs(){
	        $args="";
	        $filter="ConnectionIDs";
	        return $this->BASE->Upnp($this->SERVICEURL,$this->SERVICE,'GetCurrentConnectionIDs',$args,$filter);
	    }
	    /***************************************************************************
	    /* Funktion : GetCurrentConnectionInfo
	    /*
	    /*  Benoetigt:
	    /*          @ConnectionID (i4)
	    /*
	    /*  Liefert als Ergebnis: Array mit folgenden Keys
	    /*          @RcsID (i4)
	    /*          @AVTransportID (i4)
	    /*          @ProtocolInfo (string)
	    /*          @PeerConnectionManager (string)
	    /*          @PeerConnectionID (i4)
	    /*          @Direction (string)  => Auswahl: Input|Output
	    /*          @Status (string)  => Auswahl: OK|ContentFormatMismatch|InsufficientBandwidth|UnreliableChannel|Unknown
	    /*
	    /****************************************************************************/
	    public function GetCurrentConnectionInfo($ConnectionID){
	        $args="<ConnectionID>$ConnectionID</ConnectionID>";
	        $filter="RcsID,AVTransportID,ProtocolInfo,PeerConnectionManager,PeerConnectionID,Direction,Status";
	        return $this->BASE->Upnp($this->SERVICEURL,$this->SERVICE,'GetCurrentConnectionInfo',$args,$filter);
	    }}
}

/*##########################################################################*/
/*  Class  : RenderingControl
/*  Service: urn:schemas-upnp-org:service:RenderingControl:1
/*	     Id: urn:upnp-org:serviceId:RenderingControl
/*##########################################################################*/
if (!class_exists("CaskeidRenderingControl") ) {
class CaskeidRenderingControl extends CaskeidUpnpClass {
    protected $SERVICE='urn:schemas-upnp-org:service:RenderingControl:3';
    protected $SERVICEURL='/Control/org.mpris.MediaPlayer2.mansion/RygelRenderingControl';
    protected $EVENTURL='/Event/org.mpris.MediaPlayer2.mansion/RygelRenderingControl';
    
    public function HandleEvent($xml) {
         $instance = $this->BASE->GetInstance();
			$xml = simplexml_load_string($xml);
			#IPS_LogMessage("HandleEvent", print_r($xml, true));
			try {
            $volume = @intval((string)$xml->InstanceID[0]->Volume[0]->attributes()->val[0]);
            #IPS_LogMessage("HandleEvent", "volume" .$playstate);
            SetValue(IPS_GetObjectIDByIdent("VOLUME",$instance), $volume);
			} catch(Exception $e) {}
			try {
            $mute = @intval((string)$xml->InstanceID[0]->Mute[0]->attributes()->val[0]);
            #IPS_LogMessage("HandleEvent", "volume" .$playstate);
            #SetValue(IPS_GetObjectIDByIdent("MUTE",$instance), $mute);
			} catch(Exception $e) {}
	 }
    /***************************************************************************
    /* Funktion : GetMute
    /*
    /*  Benoetigt:
    /*          @InstanceID (ui4) Vorgabe = 0
    /*          @Channel (string) Vorgabe = 'MASTER'  => Auswahl: Master|LF|RF|SpeakerOnly
    /*
    /*  Liefert als Ergebnis:
    /*          @CurrentMute (boolean)
    /*
    /****************************************************************************/
    public function GetMute($InstanceID=0, $Channel='MASTER'){
        $args="<InstanceID>$InstanceID</InstanceID><Channel>$Channel</Channel>";
        $filter="CurrentMute";
        return $this->BASE->Upnp($this->SERVICEURL,$this->SERVICE,'GetMute',$args,$filter);
    }
    /***************************************************************************
    /* Funktion : SetMute
    /*
    /*  Benoetigt:
    /*          @InstanceID (ui4) Vorgabe = 0
    /*          @Channel (string) Vorgabe = 'MASTER'  => Auswahl: Master|LF|RF|SpeakerOnly
    /*          @DesiredMute (boolean)
    /*
    /*  Liefert als Ergebnis: Nichts
    /*
    /****************************************************************************/
    public function SetMute($InstanceID=0, $Channel='MASTER', $DesiredMute){
        $args="<InstanceID>$InstanceID</InstanceID><Channel>$Channel</Channel><DesiredMute>$DesiredMute</DesiredMute>";
        $filter="";
        return $this->BASE->Upnp($this->SERVICEURL,$this->SERVICE,'SetMute',$args,$filter);
    }
    
    /***************************************************************************
    /* Funktion : GetVolume
    /*
    /*  Benoetigt:
    /*          @InstanceID (ui4) Vorgabe = 0
    /*          @Channel (string) Vorgabe = 'MASTER'  => Auswahl: Master|LF|RF
    /*
    /*  Liefert als Ergebnis:
    /*          @CurrentVolume (ui2)
    /*
    /****************************************************************************/
    public function GetVolume($InstanceID=0, $Channel='MASTER'){
        $args="<InstanceID>$InstanceID</InstanceID><Channel>$Channel</Channel>";
        $filter="CurrentVolume";
        return $this->BASE->Upnp($this->SERVICEURL,$this->SERVICE,'GetVolume',$args,$filter);
    }
    /***************************************************************************
    /* Funktion : SetVolume
    /*
    /*  Benoetigt:
    /*          @DesiredVolume (ui2)
    /*          @InstanceID (ui4) Vorgabe = 0
    /*          @Channel (string) Vorgabe = 'MASTER'  => Auswahl: Master|LF|RF
    /*          
    /*
    /*  Liefert als Ergebnis: Nichts
    /*
    /****************************************************************************/
    public function SetVolume($DesiredVolume, $InstanceID=0, $Channel='Master'){
        $args="<InstanceID>$InstanceID</InstanceID><Channel>$Channel</Channel><DesiredVolume>$DesiredVolume</DesiredVolume>";
        $filter="";
        return $this->BASE->Upnp($this->SERVICEURL,$this->SERVICE,'SetVolume',$args,$filter);
    }
   }
}
/*##########################################################################*/
/*  Class  : AVTransport
/*  Service: urn:schemas-upnp-org:service:AVTransport:1
/*	     Id: urn:upnp-org:serviceId:AVTransport
/*##########################################################################*/
if (!class_exists("CaskeidSessionManagement")) {
    class CaskeidSessionManagement extends CaskeidUpnpClass {
    protected $SERVICE='urn:schemas-pure-com:service:SessionManagement:1';
    protected $SERVICEURL='/Control/org.mpris.MediaPlayer2.mansion/RygelSessionManagement';
    protected $EVENTURL='/Event/org.mpris.MediaPlayer2.mansion/RygelSessionManagement';

    public function HandleEvent($xml) {
         // Wir bekommen hier kein XML sondern direkt die SessionID oder einen Leerstring
			$instance = $this->BASE->GetInstance();
			if ($xml) {
				// Ist in Session und Master
				SetValue(IPS_GetObjectIDByIdent("SESSIONID",$instance), $xml);
				SetValue(IPS_GetObjectIDByIdent("SESSIONMASTER",$instance), true);
			} else {
				// Keine Session
				//SetValue(IPS_GetObjectIDByIdent("SESSIONID",$instance), "");
				SetValue(IPS_GetObjectIDByIdent("SESSIONMASTER",$instance), false);
			}
	 }

    public function CreateSession($Metadata){
        $args="<Metadata>$Metadata</Metadata>";
        $filter="";
        return $this->BASE->Upnp($this->SERVICEURL,$this->SERVICE,'CreateSession',$args,$filter);
    }

    public function DestroySession($SessionID){
        $args="<SessionID>$SessionID</SessionID>";
        $filter="";
        return $this->BASE->Upnp($this->SERVICEURL,$this->SERVICE,'DestroySession',$args,$filter);
    }

    public function GetSession(){
        $args="";
        $filter="SessionID,Metadata";
        return $this->BASE->Upnp($this->SERVICEURL,$this->SERVICE,'GetSession',$args,$filter);
    }

    public function AddUnitToSession($SessionID, $UUID){
        $args="<SessionID>$SessionID</SessionID><UUID>$UUID</UUID>";
        $filter="";
        return $this->BASE->Upnp($this->SERVICEURL,$this->SERVICE,'AddUnitToSession',$args,$filter);
    }

    public function RemoveUnitFromSession($SessionID, $UUID){
        $args="<SessionID>$SessionID</SessionID><UUID>$UUID</UUID>";
        $filter="";
        return $this->BASE->Upnp($this->SERVICEURL,$this->SERVICE,'RemoveUnitFromSession',$args,$filter);
    }
}
}
/*##########################################################################*/
/*  Class  : AVTransport
/*  Service: urn:schemas-upnp-org:service:AVTransport:1
/*	     Id: urn:upnp-org:serviceId:AVTransport
/*##########################################################################*/
if (!class_exists("CaskeidAVTransport") ) {
class CaskeidAVTransport extends CaskeidUpnpClass {
    protected $SERVICE='urn:schemas-upnp-org:service:AVTransport:1';
    protected $SERVICEURL='/Control/org.mpris.MediaPlayer2.mansion/RygelAVTransport';
    protected $EVENTURL='/Event/org.mpris.MediaPlayer2.mansion/RygelAVTransport';

    public function HandleEvent($xml) {
        $instance = $this->BASE->GetInstance();
        $xml = simplexml_load_string($xml);
        $playstate = @(string)$xml->InstanceID[0]->TransportState[0]->attributes();
        if ($playstate) {
            SetValue(IPS_GetObjectIDByIdent("PLAYSTATE",$instance), $playstate);
        }
        $curtrackuri = @(string)$xml->InstanceID[0]->CurrentTrackURI[0]->attributes();
        if ($curtrackuri) {
            SetValue(IPS_GetObjectIDByIdent("CURRENTTRACKURI",$instance), $curtrackuri);
        }
	 }
    /***************************************************************************
    /* Funktion : SetAVTransportURI
    /*
    /*  Benoetigt:
    /*          @InstanceID (ui4) Vorgabe = 0
    /*          @CurrentURI (string)
    /*          @CurrentURIMetaData (string)
    /*
    /*  Liefert als Ergebnis: Nichts
    /*
    /****************************************************************************/
    public function SetAVTransportURI($CurrentURI, $InstanceID=0, $CurrentURIMetaData=""){
        $args="<InstanceID>$InstanceID</InstanceID><CurrentURI>$CurrentURI</CurrentURI><CurrentURIMetaData>$CurrentURIMetaData</CurrentURIMetaData>";
        $filter="";
        return $this->BASE->Upnp($this->SERVICEURL,$this->SERVICE,'SetAVTransportURI',$args,$filter);
    }
    /***************************************************************************
    /* Funktion : SetNextAVTransportURI
    /*
    /*  Benoetigt:
    /*          @InstanceID (ui4) Vorgabe = 0
    /*          @NextURI (string)
    /*          @NextURIMetaData (string)
    /*
    /*  Liefert als Ergebnis: Nichts
    /*
    /****************************************************************************/
    public function SetNextAVTransportURI($InstanceID=0, $NextURI, $NextURIMetaData){
        $args="<InstanceID>$InstanceID</InstanceID><NextURI>$NextURI</NextURI><NextURIMetaData>$NextURIMetaData</NextURIMetaData>";
        $filter="";
        return $this->BASE->Upnp($this->SERVICEURL,$this->SERVICE,'SetNextAVTransportURI',$args,$filter);
    }
    /***************************************************************************
    /* Funktion : GetMediaInfo
    /*
    /*  Benoetigt:
    /*          @InstanceID (ui4) Vorgabe = 0
    /*
    /*  Liefert als Ergebnis: Array mit folgenden Keys
    /*          @NrTracks (ui4)
    /*          @MediaDuration (string)
    /*          @CurrentURI (string)
    /*          @CurrentURIMetaData (string)
    /*          @NextURI (string)
    /*          @NextURIMetaData (string)
    /*          @PlayMedium (string)  => Auswahl: NONE|NETWORK
    /*          @RecordMedium (string)  => Auswahl: NONE
    /*          @WriteStatus (string)
    /*
    /****************************************************************************/
    public function GetMediaInfo($InstanceID=0){
        $args="<InstanceID>$InstanceID</InstanceID>";
        $filter="NrTracks,MediaDuration,CurrentURI,CurrentURIMetaData,NextURI,NextURIMetaData,PlayMedium,RecordMedium,WriteStatus";
        return $this->BASE->Upnp($this->SERVICEURL,$this->SERVICE,'GetMediaInfo',$args,$filter);
    }
    /***************************************************************************
    /* Funktion : GetTransportInfo
    /*
    /*  Benoetigt:
    /*          @InstanceID (ui4) Vorgabe = 0
    /*
    /*  Liefert als Ergebnis: Array mit folgenden Keys
    /*          @CurrentTransportState (string)  => Auswahl: STOPPED|PLAYING|PAUSED_PLAYBACK|TRANSITIONING
    /*          @CurrentTransportStatus (string)
    /*          @CurrentSpeed (string)  => Auswahl: 1
    /*
    /****************************************************************************/
    public function GetTransportInfo($InstanceID=0){
        $args="<InstanceID>$InstanceID</InstanceID>";
        $filter="CurrentTransportState,CurrentTransportStatus,CurrentSpeed";
        return $this->BASE->Upnp($this->SERVICEURL,$this->SERVICE,'GetTransportInfo',$args,$filter);
    }
    /***************************************************************************
    /* Funktion : GetPositionInfo
    /*
    /*  Benoetigt:
    /*          @InstanceID (ui4) Vorgabe = 0
    /*
    /*  Liefert als Ergebnis: Array mit folgenden Keys
    /*          @Track (ui4)
    /*          @TrackDuration (string)
    /*          @TrackMetaData (string)
    /*          @TrackURI (string)
    /*          @RelTime (string)
    /*          @AbsTime (string)
    /*          @RelCount (i4)
    /*          @AbsCount (i4)
    /*
    /****************************************************************************/
    public function GetPositionInfo($InstanceID=0){
        $args="<InstanceID>$InstanceID</InstanceID>";
        $filter="Track,TrackDuration,TrackMetaData,TrackURI,RelTime,AbsTime,RelCount,AbsCount";
        return $this->BASE->Upnp($this->SERVICEURL,$this->SERVICE,'GetPositionInfo',$args,$filter);
    }
    /***************************************************************************
    /* Funktion : GetDeviceCapabilities
    /*
    /*  Benoetigt:
    /*          @InstanceID (ui4) Vorgabe = 0
    /*
    /*  Liefert als Ergebnis: Array mit folgenden Keys
    /*          @PlayMedia (string)
    /*          @RecMedia (string)
    /*          @RecQualityModes (string)
    /*
    /****************************************************************************/
    public function GetDeviceCapabilities($InstanceID=0){
        $args="<InstanceID>$InstanceID</InstanceID>";
        $filter="PlayMedia,RecMedia,RecQualityModes";
        return $this->BASE->Upnp($this->SERVICEURL,$this->SERVICE,'GetDeviceCapabilities',$args,$filter);
    }
    /***************************************************************************
    /* Funktion : GetTransportSettings
    /*
    /*  Benoetigt:
    /*          @InstanceID (ui4) Vorgabe = 0
    /*
    /*  Liefert als Ergebnis: Array mit folgenden Keys
    /*          @PlayMode (string)  => Auswahl: NORMAL|REPEAT_ALL|SHUFFLE_NOREPEAT|SHUFFLE
    /*          @RecQualityMode (string)
    /*
    /****************************************************************************/
    public function GetTransportSettings($InstanceID=0){
        $args="<InstanceID>$InstanceID</InstanceID>";
        $filter="PlayMode,RecQualityMode";
        return $this->BASE->Upnp($this->SERVICEURL,$this->SERVICE,'GetTransportSettings',$args,$filter);
    }
    /***************************************************************************
    /* Funktion : Stop
    /*
    /*  Benoetigt:
    /*          @InstanceID (ui4) Vorgabe = 0
    /*
    /*  Liefert als Ergebnis: Nichts
    /*
    /****************************************************************************/
    public function Stop($InstanceID=0){
        $args="<InstanceID>$InstanceID</InstanceID>";
        $filter="";
        return $this->BASE->Upnp($this->SERVICEURL,$this->SERVICE,'Stop',$args,$filter);
    }
    
    public function SyncStop($InstanceID=0){
        $args="<InstanceID>$InstanceID</InstanceID><StopTime></StopTime><ReferenceClockId></ReferenceClockId>";
        $filter="";
        return $this->BASE->Upnp($this->SERVICEURL,$this->SERVICE,'SyncStop',$args,$filter);
    }
    /***************************************************************************
    /* Funktion : Play
    /*
    /*  Benoetigt:
    /*          @InstanceID (ui4) Vorgabe = 0
    /*          @Speed (string) Vorgabe = 1  => Auswahl: 1
    /*
    /*  Liefert als Ergebnis: Nichts
    /*
    /****************************************************************************/
    public function Play($InstanceID=0, $Speed=1){
        $args="<InstanceID>$InstanceID</InstanceID><Speed>$Speed</Speed>";
        $filter="";
        return $this->BASE->Upnp($this->SERVICEURL,$this->SERVICE,'Play',$args,$filter);
    }
    /***************************************************************************
    /* Funktion : SyncPlay
    /*
    /*  Benoetigt:
    /*          @InstanceID (ui4) Vorgabe = 0
    /*          @Speed (string) Vorgabe = 1  => Auswahl: 1
    /*
    /*  Liefert als Ergebnis: Nichts
    /*
    /****************************************************************************/
    public function SyncPlay($InstanceID=0, $Speed=1){
        $args="<InstanceID>$InstanceID</InstanceID><Speed>$Speed</Speed><ReferencePositionUnits>RelTime</ReferencePositionUnits><ReferenceClockId>DeviceClockId</ReferenceClockId><ReferencePosition></ReferencePosition><ReferencePresentationTime></ReferencePresentationTime>";
        $filter="";
        return $this->BASE->Upnp($this->SERVICEURL,$this->SERVICE,'SyncPlay',$args,$filter);
    }
    /***************************************************************************
    /* Funktion : Pause
    /*
    /*  Benoetigt:
    /*          @InstanceID (ui4) Vorgabe = 0
    /*
    /*  Liefert als Ergebnis: Nichts
    /*
    /****************************************************************************/
    public function Pause($InstanceID=0){
        $args="<InstanceID>$InstanceID</InstanceID>";
        $filter="";
        return $this->BASE->Upnp($this->SERVICEURL,$this->SERVICE,'Pause',$args,$filter);
    }
    /***************************************************************************
    /* Funktion : Seek
    /*
    /*  Benoetigt:
    /*          @InstanceID (ui4) Vorgabe = 0
    /*          @Unit (string)  => Auswahl: TRACK_NR|REL_TIME|SECTION
    /*          @Target (string)
    /*
    /*  Liefert als Ergebnis: Nichts
    /*
    /****************************************************************************/
    public function Seek($Target, $Unit="REL_TIME", $InstanceID=0){
        $args="<InstanceID>$InstanceID</InstanceID><Unit>$Unit</Unit><Target>$Target</Target>";
        $filter="";
        return $this->BASE->Upnp($this->SERVICEURL,$this->SERVICE,'Seek',$args,$filter);
    }
    /***************************************************************************
    /* Funktion : Next
    /*
    /*  Benoetigt:
    /*          @InstanceID (ui4) Vorgabe = 0
    /*
    /*  Liefert als Ergebnis: Nichts
    /*
    /****************************************************************************/
    public function Next($InstanceID=0){
        $args="<InstanceID>$InstanceID</InstanceID>";
        $filter="";
        return $this->BASE->Upnp($this->SERVICEURL,$this->SERVICE,'Next',$args,$filter);
    }
    /***************************************************************************
    /* Funktion : Previous
    /*
    /*  Benoetigt:
    /*          @InstanceID (ui4) Vorgabe = 0
    /*
    /*  Liefert als Ergebnis: Nichts
    /*
    /****************************************************************************/
    public function Previous($InstanceID=0){
        $args="<InstanceID>$InstanceID</InstanceID>";
        $filter="";
        return $this->BASE->Upnp($this->SERVICEURL,$this->SERVICE,'Previous',$args,$filter);
    }}
}
?>
