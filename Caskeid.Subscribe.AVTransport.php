<?
/*****************************
*
* Caskeid.Subscribe.AVTransport.php
*
* Baut Subscribtionen auf zu den AVTransport Services.
* Muss mind. alle 5 Minuten wiederholt werden.
*
* (c) 2015 Jens-Michael Cramer
*
* Version: 1.0
*
******************************/
set_time_limit(120);
require_once("caskeid.class.php");


#$callback_url = "http://192.168.11.4/avtransport.php";
$callback_url = "http://192.168.11.23:82/hook/caskeid_avtrans";

$speaker = IPS_GetChildrenIDs(CaskeidUpnpDevice::getDeviceFolderID());

foreach($speaker as $s) {
	$ip   = GetValueString(IPS_GetObjectIDByIdent("IP",$s));
	$port = GetValueString(IPS_GetObjectIDByIdent("PORT",$s));
	
	$box = new CaskeidUpnpDevice("http://".$ip.":".$port);
	try {
		$box->CallService('AVTransport','UnRegisterEventCallback',"");
	} catch (Exception $e) {}
	try {
		$box->CallService('AVTransport','RegisterEventCallback',array($callback_url,300));
	} catch (Exception $e) {}
}
?>
